/*
 Navicat Premium Data Transfer

 Source Server         : carrentsystem
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : rentcarsystem

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 13/11/2022 20:25:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for usersinfo
-- ----------------------------
DROP TABLE IF EXISTS `usersinfo`;
CREATE TABLE `usersinfo`  (
  `id` int NULL DEFAULT NULL,
  `account` int NULL DEFAULT NULL,
  `passwords` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `identify` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `money` double NULL DEFAULT NULL,
  `IncomeOfSystem` int NULL DEFAULT NULL COMMENT '系统总营业额，仅供管理员查询'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of usersinfo
-- ----------------------------
INSERT INTO `usersinfo` VALUES (1, 111, 'CO_1', 'VIP', 7396.080078125, NULL);
INSERT INTO `usersinfo` VALUES (2, 222, 'CV_1', 'VIP', 4128, NULL);
INSERT INTO `usersinfo` VALUES (0, 1006, 'swpu', 'admin', 12986, NULL);
INSERT INTO `usersinfo` VALUES (4, 333, 'CO_2', 'ordinary', 699.92, NULL);
INSERT INTO `usersinfo` VALUES (5, 444, 'CV_2', 'VIP', 65666, NULL);
INSERT INTO `usersinfo` VALUES (6, 555, 'test', 'VIP', 89770, NULL);
INSERT INTO `usersinfo` VALUES (7, 999, 'test', 'VIP', 68920, NULL);

SET FOREIGN_KEY_CHECKS = 1;
