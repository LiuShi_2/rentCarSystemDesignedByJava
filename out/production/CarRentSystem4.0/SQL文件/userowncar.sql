/*
 Navicat Premium Data Transfer

 Source Server         : carrentsystem
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : rentcarsystem

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 13/11/2022 20:25:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for userowncar
-- ----------------------------
DROP TABLE IF EXISTS `userowncar`;
CREATE TABLE `userowncar`  (
  `ownerId` int NULL DEFAULT NULL COMMENT '不同用户的身份标识，同一用户租的车拥有相同userId\r\n',
  `vehicle` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `brand` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `type` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `nums` int NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of userowncar
-- ----------------------------
INSERT INTO `userowncar` VALUES (NULL, NULL, NULL, NULL, NULL);
INSERT INTO `userowncar` VALUES (2, '轿车', '大众', '川A-0004', 1);
INSERT INTO `userowncar` VALUES (2, '货车', '大卡three-限载120t', '川B-0011', 2);
INSERT INTO `userowncar` VALUES (2, '货车', '大卡three-限载120t', '川B-0011', 2);
INSERT INTO `userowncar` VALUES (4, '货车', '大卡two-限载80t', '川B-0010', 1);
INSERT INTO `userowncar` VALUES (4, '轿车', '保时捷', '川A-0002', 1);
INSERT INTO `userowncar` VALUES (4, '客车', '金龙-25座', '川A-0007', 2);
INSERT INTO `userowncar` VALUES (1, '货车', '大卡three-限载120t', '川B-0011', 1);
INSERT INTO `userowncar` VALUES (2, '客车', '金杯-30座', '川B-0006', 3);
INSERT INTO `userowncar` VALUES (1, '客车', '金杯-30座', '川B-0006', 1);
INSERT INTO `userowncar` VALUES (7, '客车', '金杯-20座', '川B-0005', 3);
INSERT INTO `userowncar` VALUES (7, '货车', '大卡two-限载80t', '川B-0010', 1);

SET FOREIGN_KEY_CHECKS = 1;
