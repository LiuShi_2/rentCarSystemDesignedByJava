package CarRentalSystem.menuMsg;

public class menuMsg {
    //打印系统欢迎语
    public static void printWelcome() {
        System.out.println("\n☆☆☆☆ 欢迎光临暮商汽车租赁系统  --Designed by 杨荣兴 ☆☆☆☆\n");
    }

    //登录提示
    public static void Print_Login_Info() {
        System.out.println("请输入密码登录租车系统");
    }

    public static void Login_user() {
        System.out.print("用户账号");
    }

    public static void Login_passwords() {
        System.out.print("用户密码");
    }


    /*
     * 后台相关
     * */
    //进入后台欢迎语
    public static void helloAdmin() {
        System.out.println("\n尊敬的管理员您好，欢迎进入租车系统后台");
    }

    //后台操作菜单
    public static void adminMenu() {
        printWelcome();
        System.out.print("输入编号，选择您的将要进行的操作: \n-1、退出登录\n 0、退出系统  \n 1、查看系统车辆信息\n 2、查看营业额  \n 3、新增车辆\n 4、删除车辆  \n 5、查看当前用户信息\n 6、修改当前用户权限  \n 7、补货\n 8、更新车辆日租金 ");
    }

    //退出系统提示
    public static void sayBye() {
        System.out.println("正在退出系统...\n已退出系统，欢迎下次光临~~~~");
    }

    //新增车辆
    public static void readyToAddVehicle() {
        System.out.println("请输入新增车辆信息\n请选择新增车辆类型: 1、客车  2、轿车  3、货车");
    }

    //输入新增车辆信息:
    public static void setNewCarBrand() {
        System.out.print("输入新增车辆的品牌: ");
    }
    public static void setNewCarType(){System.out.print("请输入新增车辆车牌号: ");}

    public static void setNewCarSeats() {
        System.out.print("输入新增车辆的座位数: ");
    }

    public static void setNewCarLoad() {
        System.out.print("输入新增货车的限载量: ");
    }
    public static void setNewCarRentdaily(){System.out.print("请输入新增车辆日租金: ");}
    public static void setNewCarNums(){
        System.out.print("请输入新增车辆的数量: ");
    }

    //输入新增车辆
    public static void NewInfoAddOver(){
        System.out.println("信息录入成功，新车已上架");
    }
    public static void NewInfoAddError(){
        System.out.println("遇到错误，车辆信息录入失败，请稍后重试");
    }

    //修改现有车辆信息
    public static void ShowNowInfo(){
        System.out.println("\n当前系统车辆信息如下: ");
    }

    public static void changeWhichCar(){
        System.out.println("请选择车辆,补充库存:");
    }
    //public static void giveNewInfo(){System.out.print("请输入需要增加库存的车辆的编号以及增加的数量: ");}
    public static void changeWhichCarPreRent(){
        System.out.printf("请输入需要修改日租金的车辆的编号和新的日租金: ");
    }


    public static void changeOver(){
        System.out.println("更新成功~");
    }

    //删除车辆
    public static void deleteCar(){System.out.printf("请选择您要删除的车辆类型: 1、轿车  2、客车  3、货车  或输入其他任意数字取消删除");}
    //public static void deleteCarTypeInfo(){System.out.println("以下是系统中当前您所选择的车类库：");}
    public static void deleteThatCar(){System.out.printf("请输入要删除车辆的编号: ");}
    public static void deleteOver(){System.out.println("车辆已删除");}
    public static void deleteError(){
        System.out.println("删除失败，请检查代码是否存在问题");
    }

    //查看以及修改用户信息或权限
    public static void seeUsersInfo(){System.out.println("当前系统内的用户信息如下: ");}
    public static void chooseUser(){System.out.println("选择您要修改权限的用户:");}
    public static void changeWhichInfoSucess(){System.out.println("用户权限修改成功！");}

    /*
    * 用户界面
    * 分为普通用户和VIP用户
    * */
    //用户查看余额以及充值
    public static void showMoney(int money){System.out.printf("您当前账户中余额为: "+money);}
    public static void addMoney(){System.out.println("请输入你要充值的金额");}
    public static void addMoneyOver(float money){System.out.println("充值成功，您当前账户余额为: "+money);}

    //租车结束提示余额

    public static void rentOver(float rent, int rentSum, float balance){System.out.println("\n租车结束!一共租车"+rentSum+"辆， 付款"+rent+"元, 您的账户余额"+balance+"元");}
    //余额不足！
    public static void moneyNotEnough(){System.out.println("您的账户余额不足！请充值后再继续您的操作，系统即将回到主界面！");}

    //租车选择
    public static void choiceCarToRent(){
        System.out.println("请按顺序依次输入您想要租的车的编号、需要租的数量");
    }

    /*
    * 注册账号
    * */
    public static void registerLead(){
        System.out.println("欢迎注册账号，请按照以下提示输入您的账号信息，并在账户中存入一定资金");
    }

    public static void registerAccountLead(){
        System.out.printf("请输入您想要注册的账号 (必须为整数): ");
    }

    public static void registerPasswordsLead(){
        System.out.printf("请设置您的密码 (不超过10个字符): ");
    }

    public static void registerSetMoney(){
        System.out.printf("请在账户中存入一定资金，如果想要成为VIP客户，享受折扣以及其他尊贵待遇，稍后可以花费10000元办理相关业务\n若您暂时不想要在账户上存款，您也可以输入0，稍后进入系统再选择充值: ");
    }

    public static void resgisterChooseIdentify(){
        System.out.printf("尊敬的用户，您是否需要花费10000元开通VIP服务？输入 1 开通， 0 不开通: ");
    }

    public static void becomeVIP(){
        System.out.println("尊敬的用户，您已成为VIP顾客，享受8折租车、账户租车大幅上限提升等特权！\n请重新登录，享受VIP尊贵服务");
    }


    public static void registerSuccess(int account, String passwords){
        System.out.println("账号注册成功！请牢记您的账号: "+account+", 密码: "+passwords+"\n正在进入系统......loading......\n请登录您注册的账号");
    }

    public static void registerError(){
        System.out.println("您输入的账号或密码不符合要求, 请按照系统提示重新注册!");
    }


}
