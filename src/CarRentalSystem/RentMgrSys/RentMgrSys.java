package CarRentalSystem.RentMgrSys;
import CarRentalSystem.JDBCUtils.*;
import CarRentalSystem.loginAndRegister.*;
import CarRentalSystem.admin.*;
import CarRentalSystem.costomer.*;
import CarRentalSystem.menuMsg.*;
import java.util.*;
import java.sql.*;


public class RentMgrSys {
    public static int usersNums=1;
    public static int carNums=1;
    public static Connection conn=null;
    public static Statement stmt = null;    //执行sql语句对象
    public static ResultSet rs = null;      //结果集对象
    public static Scanner in = new Scanner(System.in);
    public static boolean isOver=false;
    public static boolean logOut=false;

    public static void main(String[] args){

        //数据库连接
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql:///carrentsystem?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2B8", "root", "yrx000000");
        }
        catch (SQLException e) {
            System.out.println("数据库连接出现异常");
            e.printStackTrace();
        }

        //对象准备
        try {
            RentMgrSys.stmt = conn.createStatement();
        }catch (SQLException e){
            System.out.println("执行sql语句对象获取出现异常");
            e.printStackTrace();
        }
        //数据和数据库同步
        try {
            rs = stmt.executeQuery("select count(id) from cardata");
            if(rs.next()){
                RentMgrSys.carNums = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            rs = stmt.executeQuery("select count(id) from usersinfo");
            if(rs.next()){
                RentMgrSys.usersNums = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


/***********************数据库连接结束，以下是用户操作***********************************/
        int account=0;
        String passwords="", c="";
        int LoginOrRegister;

        while (true){
            RentMgrSys.logOut=false;
            menuMsg.printWelcome();
            System.out.println("1、登录\t2、注册\t0、输入其他任意数字退出系统");
            LoginOrRegister=RentMgrSys.in.nextInt();

            switch (LoginOrRegister){
                case 1:
                    //账号输入与判断
                    while(true){
                        menuMsg.printWelcome();
                        menuMsg.Print_Login_Info();
                        menuMsg.Login_user();
                        account = in.nextInt(); // 输入账号
                        c=in.nextLine();        // 接收\n
                        menuMsg.Login_passwords();
                        passwords = in.nextLine();

                        Login.LoginJustifyResult loginResult = Login.isLoginSuccess(account, passwords, stmt, rs);
                        if (loginResult.getCount() == 0){
                            RentMgrSys.logOut=true;
                        }else if(loginResult.getIsTrue() == 0){
                            continue;
                        }else if(loginResult.getIsTrue() == 1){
                            try {
                                RentMgrSys.rs = RentMgrSys.stmt.executeQuery(jdbcUtilsByAdmin.sqlSearchUserSpecifyInfo("*", loginResult.getUserId()));
                                RentMgrSys.rs.next();

                                switch (RentMgrSys.rs.getString("identify")) {
                                    case "ordinary":
                                        ordinaryAboutCostomer OC = new ordinaryAboutCostomer(RentMgrSys.rs.getInt("id"), RentMgrSys.rs.getString("identify"), RentMgrSys.rs.getFloat("money"));
                                        OC.myCarDataSync(OC);
                                        OC.userMenu();
                                        break;
                                    case "VIP":
                                        VIPAboutCostomer VC = new VIPAboutCostomer(RentMgrSys.rs.getInt("id"), RentMgrSys.rs.getString("identify"), RentMgrSys.rs.getFloat("money"));
                                        VC.myCarDataSync(VC);
                                        VC.userMenu();
                                        break;
                                    case "admin":
                                        new admin();
                                        break;
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }

                        if(RentMgrSys.isOver || RentMgrSys.logOut){
                            break;
                        }
                    }
                    break;
                case 2:
                    Register.beginRegister();
                    break;
                default:
                    RentMgrSys.isOver=true;
                    break;
            }

            if(RentMgrSys.isOver){
                break;
            }
        }



        //断开数据库连接
        DBLinkOver.Over();
        menuMsg.sayBye();
    }
}
