package CarRentalSystem.admin;
import CarRentalSystem.RentMgrSys.RentMgrSys;
import CarRentalSystem.menuMsg.menuMsg;
import CarRentalSystem.JDBCUtils.jdbcUtilsByAdmin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class admin {
    Scanner in = new Scanner(System.in);
    int newCarKind;
    private String newBrand = "";
    private String newType = "";
    private String newVehicle = "";
    private int perRent = 0;
    private int newBusSeats = 0;
    private int newSeats = 0;
    private int newLoad = 0;
    private int deleteCarType = 0;
    private int theDeleteCar;
    private int id = 12;
    private int newCarNums=0;
    private int feature=0;
    private double income = 0;//初始营业额为0,每次管理登录时从数据库中读取营业额更新，这里的income用作查看与像数据库传值

    public int getFeature() {
        return feature;
    }

    public void setFeature(int feature) {
        this.feature = feature;
    }

    //setter 和 getter
    public int getPerRent() {
        return perRent;
    }

    public void setPerRent(int perRent) {
        this.perRent = perRent;
    }

    public int getNewBusSeats() {
        return newBusSeats;
    }

    public void setNewBusSeats(int newBusSeats) {
        this.newBusSeats = newBusSeats;
    }

    public int getNewLoad() {
        return newLoad;
    }

    public void setNewLoad(int newLoad) {
        this.newLoad = newLoad;
    }

    public int getDeleteCarType() {
        return deleteCarType;
    }

    public void setDeleteCarType(int deleteCarType) {
        this.deleteCarType = deleteCarType;
    }

    public int getTheDeleteCar() {
        return theDeleteCar;
    }

    public void setTheDeleteCar(int theDeleteCar) {
        this.theDeleteCar = theDeleteCar;
    }

    public void deleteCarMethod(){}

    public String getNewBrand() {
        return newBrand;
    }

    public void setNewBrand(String newBrand) {
        this.newBrand = newBrand;
    }

    public String getNewType() {
        return newType;
    }

    public void setNewType(String newType) {
        this.newType = newType;
    }

    public String getNewVehicle() {
        return newVehicle;
    }

    public void setNewVehicle(String newVehicle) {
        this.newVehicle = newVehicle;
    }

    public int getNewSeats() {
        return newSeats;
    }

    public void setNewSeats(int seats) {
        this.newSeats = newSeats;
    }

    public double getIncome(){
        return income;
    }
    public void setIncome(double rent){
        this.income += rent;
    }

    public int getNewCarNums() {
        return newCarNums;
    }

    public void setNewCarNums(int newCarNums) {
        this.newCarNums = newCarNums;
    }

    public static void relenishment(int id, int addNums){
        try {
            RentMgrSys.rs = RentMgrSys.stmt.executeQuery(jdbcUtilsByAdmin.sqlSearchCarSpecifyInfo("nums", id));
            RentMgrSys.rs.next();
            int numsBefore = RentMgrSys.rs.getInt("nums");
            RentMgrSys.stmt.executeUpdate("update cardata set nums="+numsBefore+addNums+" where id="+id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public int getId() {
        return id;
    }

    public void setId(int num) {
        this.id = num;
    }

    public static void showCarsInSystemNow(){
        try {
            RentMgrSys.rs = RentMgrSys.stmt.executeQuery("select * from cardata");
            RentMgrSys.rs.absolute(1);

            do {
                System.out.println("编号: "+RentMgrSys.rs.getInt("id")+"\t\t车类: "+RentMgrSys.rs.getString("vehicle")+"\t\t品牌: "+RentMgrSys.rs.getString("brand")+"\t\t\t车牌号: "+RentMgrSys.rs.getString("type")+"\t\t\t日租金: "+RentMgrSys.rs.getInt("preRent")+"\t\t\t当前系统存量: "+RentMgrSys.rs.getInt("nums"));
            }while(RentMgrSys.rs.next());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void showSpecificKindCarsInfo(ResultSet rs){
        try {
            rs.last();
            int row = rs.getRow();
            if(row == 0){
                System.out.println("系统中没有此类车");
                return;
            }

            rs.absolute(1);
            String strVehicle="", strBrand="", strType="", strId="", strPreRent="", strNums="";
            do{
                strId = "编号: "+rs.getInt("id");
                strVehicle = "分类: "+rs.getString("vehicle");
                strBrand = "品牌: "+rs.getString("brand");
                strType = "车牌号: "+rs.getString("type");
                strPreRent = "日租金: "+rs.getInt("preRent");
                strNums = "存量: "+rs.getInt("nums");

                strId = String.format("%-25s", strId);
                strVehicle = String.format("%-25s", strVehicle);
                strBrand = String.format("%-25s", strBrand);
                strType = String.format("%-25s", strType);
                strPreRent = String.format("%-25s", strPreRent);
                strNums = String.format("%-25s", strNums);

                System.out.println(strId+strVehicle+strBrand+strType+strPreRent+strNums);
            }while(rs.next());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteWhichCar(String carKind){
        try {
            RentMgrSys.rs = RentMgrSys.stmt.executeQuery(jdbcUtilsByAdmin.sqlReadSpecificKindCar(carKind));

            menuMsg.deleteThatCar();
            int id = RentMgrSys.in.nextInt();
            int count = RentMgrSys.stmt.executeUpdate(jdbcUtilsByAdmin.sqlDeleteCar(id));

            if(id == 1){
                System.out.println("车辆删除成功");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void adminSystem(){
        int choice;
        menuMsg.helloAdmin();
        while(true){
            menuMsg.adminMenu();
            choice = RentMgrSys.in.nextInt();
            switch (choice){
                case -1:
                    RentMgrSys.logOut=true;
                    break;
                case 0:
                    RentMgrSys.isOver=true;
                    break;
                case 1:
                    menuMsg.ShowNowInfo();
                    admin.showCarsInSystemNow();
                    //meauMsg.adminMenu();
                    break;
                case 2:
                    try {
                        RentMgrSys.rs = RentMgrSys.stmt.executeQuery(jdbcUtilsByAdmin.sqlReadMoneyNow(0));
                        RentMgrSys.rs.next();
                        System.out.println("截止目前，您的收人为："+RentMgrSys.rs.getFloat("money")+"元");
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    //meauMsg.adminMenu();
                    break;
                case 3:
                    menuMsg.readyToAddVehicle();
                    newCarKind = in.nextInt();
                    String c="";
                    //c = RentMgrSys.in.nextLine();
                    if(newCarKind == 1){
                        //vehicle-车的名牌（大类，比如轿车、客车）、brand（车的品牌， 比如玛莎拉蒂）、type（车牌号）、preRent（日租金）
                        //meauMsg.setNewVihicle();
                        setNewVehicle("客车");
                        menuMsg.setNewCarBrand();
                        setNewBrand(in.next());
                        menuMsg.setNewCarType();
                        setNewType(in.next());
                        menuMsg.setNewCarRentdaily();
                        setPerRent(in.nextInt());
                        menuMsg.setNewCarNums();
                        setNewCarNums(in.nextInt());
                        System.out.print("请设置客车限载量: ");
                        setFeature(RentMgrSys.in.nextInt());
                        try {
                            int count = RentMgrSys.stmt.executeUpdate(jdbcUtilsByAdmin.sqlAddCar(++RentMgrSys.carNums, getNewVehicle(), getNewBrand(), getNewType(), getPerRent(), getNewCarNums(), getFeature()));
                            if (count == 1){
                                menuMsg.NewInfoAddOver();
                            }
                            else {
                                menuMsg.NewInfoAddError();
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }else if(newCarKind == 2){
                        //meauMsg.setNewVihicle();
                        setNewVehicle("轿车");
                        menuMsg.setNewCarBrand();
                        setNewBrand(in.next());
                        menuMsg.setNewCarType();
                        setNewType(in.next());
                        menuMsg.setNewCarRentdaily();
                            //c=RentMgrSys.in.nextLine();
                        setPerRent(in.nextInt());
                        menuMsg.setNewCarSeats();
                        setNewSeats(in.nextInt());
                        menuMsg.setNewCarNums();
                        setNewCarNums(in.nextInt());

                        try {
                            System.out.println(RentMgrSys.carNums);
                            int count = RentMgrSys.stmt.executeUpdate(jdbcUtilsByAdmin.sqlAddCar(++RentMgrSys.carNums, getNewVehicle(),  getNewBrand(), getNewType(), getPerRent(), getNewCarNums(), -1));
                            System.out.println(RentMgrSys.carNums);
                            if (count == 1){
                                menuMsg.NewInfoAddOver();
                            }
                            else {
                                menuMsg.NewInfoAddError();
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }else if(newCarKind == 3){
                        //meauMsg.setNewVihicle();
                        setNewVehicle("货车");
                        menuMsg.setNewCarBrand();
                        setNewBrand(in.next());
                        menuMsg.setNewCarType();
                        setNewType(in.next());
                        menuMsg.setNewCarRentdaily();
                        setPerRent(in.nextInt());
                        menuMsg.setNewCarLoad();
                        setNewLoad(in.nextInt());
                        menuMsg.setNewCarNums();
                        setNewCarNums(in.nextInt());
                        System.out.print("请设置货车限载量: ");
                        setFeature(RentMgrSys.in.nextInt());
                        try {
                            int count = RentMgrSys.stmt.executeUpdate(jdbcUtilsByAdmin.sqlAddCar(++RentMgrSys.carNums, getNewVehicle(), getNewBrand(), getNewType(), getPerRent(), getNewCarNums(), getFeature()));
                            if (count == 1){
                                menuMsg.NewInfoAddOver();
                            }
                            else {
                                menuMsg.NewInfoAddError();
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }else{
                        System.out.println("您输入的选项不合法，请检查后重新输入！");
                        //adminSystem();
                    }
                    break;
                case 4:
                    menuMsg.deleteCar();
                    deleteCarType = in.nextInt();
                    switch (deleteCarType){
                        case 1:
                            try {
                                RentMgrSys.rs = RentMgrSys.stmt.executeQuery(jdbcUtilsByAdmin.sqlReadSpecificKindCar("轿车"));
                                showSpecificKindCarsInfo(RentMgrSys.rs);
                                menuMsg.deleteThatCar();
                                int thatCar = RentMgrSys.in.nextInt();
                                String deleteBrand = "";
                                RentMgrSys.rs = RentMgrSys.stmt.executeQuery("select brand from cardata where id="+thatCar);
                                if(RentMgrSys.rs.next()){
                                    deleteBrand = RentMgrSys.rs.getString("brand");
                                }

                                RentMgrSys.rs = RentMgrSys.stmt.executeQuery("select * from userowncar where brand='"+deleteBrand+"'");
                                int row=0;
                                if(RentMgrSys.rs.next()){
                                    row = RentMgrSys.rs.getRow();
                                }

                                if(row == 0){

                                    int count = RentMgrSys.stmt.executeUpdate(jdbcUtilsByAdmin.sqlDeleteCar(thatCar));
                                    if(count == 1){
                                        System.out.println("车辆已下架");
                                    }
                                }else{
                                    System.out.println("该款车辆正在被租用，无法删除\n");
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 2:
                            try {
                                RentMgrSys.rs = RentMgrSys.stmt.executeQuery(jdbcUtilsByAdmin.sqlReadSpecificKindCar("客车"));
                                showSpecificKindCarsInfo(RentMgrSys.rs);
                                menuMsg.deleteThatCar();
                                int thatCar = RentMgrSys.in.nextInt();
                                int count = RentMgrSys.stmt.executeUpdate(jdbcUtilsByAdmin.sqlDeleteCar(thatCar));
                                if(count == 1){
                                    System.out.println("车辆已下架");
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 3:
                            try {
                                RentMgrSys.rs = RentMgrSys.stmt.executeQuery(jdbcUtilsByAdmin.sqlReadSpecificKindCar("货车"));
                                showSpecificKindCarsInfo(RentMgrSys.rs);
                                menuMsg.deleteThatCar();
                                int thatCar = RentMgrSys.in.nextInt();
                                int count = RentMgrSys.stmt.executeUpdate(jdbcUtilsByAdmin.sqlDeleteCar(thatCar));
                                if(count == 1){
                                    System.out.println("车辆已下架");
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            break;
                    }
                    break;
                case 5://查看用户当前信息
                    menuMsg.seeUsersInfo();
                    try {
                        RentMgrSys.rs = RentMgrSys.stmt.executeQuery("select * from usersinfo where id!=0");
                        jdbcUtilsByAdmin.showUsersAll(RentMgrSys.rs);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    //jdbcUtilsByAdmin.showUsersAll(RentMgrSys.rs);
                    break;
                case 6://修改用户权限
                    menuMsg.seeUsersInfo();
                    try {
                        RentMgrSys.rs = RentMgrSys.stmt.executeQuery("select * from usersinfo");
                        jdbcUtilsByAdmin.showUsersAll(RentMgrSys.rs);

                        System.out.println("请输入权限需要被修改的用户的编号: ");
                        int id = RentMgrSys.in.nextInt();
                        RentMgrSys.stmt.executeUpdate(jdbcUtilsByAdmin.sqlChangeIdentify(id));
                        menuMsg.changeWhichInfoSucess();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;
                case 7:
                    menuMsg.ShowNowInfo();
                    admin.showCarsInSystemNow();
                    menuMsg.changeWhichCar();
                    int id = RentMgrSys.in.nextInt();
                    int addNums = RentMgrSys.in.nextInt();

                    try {
                        RentMgrSys.rs = RentMgrSys.stmt.executeQuery("select nums from cardata where id="+id);
                        RentMgrSys.rs.next();
                        int count = RentMgrSys.stmt.executeUpdate(jdbcUtilsByAdmin.sqlChangeWhichCarNums(id, RentMgrSys.rs.getInt("nums")+addNums));

                        if(count == 1){
                            menuMsg.changeOver();
                        }

                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;
                case 8:
                    menuMsg.ShowNowInfo();
                    admin.showCarsInSystemNow();
                    menuMsg.changeWhichCarPreRent();
                    int carId = RentMgrSys.in.nextInt();
                    int newPreRent = RentMgrSys.in.nextInt();

                    try {
                        int count = RentMgrSys.stmt.executeUpdate(jdbcUtilsByAdmin.sqlChangeWhichCarPreRent(carId, newPreRent));

                        if(count == 1){
                            menuMsg.changeOver();
                        }

                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    System.out.println("请输入有效的指令！");
                    break;
            }

            if(choice == 0 || choice == -1){
                break;
            }
        }
    }

    public admin(){
        adminSystem();
    }

}
