package CarRentalSystem.costomer;

import CarRentalSystem.CarInfo.Vehicle;

import java.sql.SQLException;
import java.util.ArrayList;
import CarRentalSystem.JDBCUtils.*;
import CarRentalSystem.RentMgrSys.RentMgrSys;
import CarRentalSystem.admin.admin;
import CarRentalSystem.menuMsg.*;

public class ordinaryAboutCostomer extends User implements aboutCostomerMenu {
    public ordinaryAboutCostomer(int userId, String identity, float money){       //创建普通用户账户
        super(userId, identity, money);
    }


    @Override                                                     //查看用户当前车辆信息
    public void seeMyCarsDetails(ArrayList<? extends Vehicle> myCar, int i){
        if(myCar.size() == 0){
            return;
        }

        boolean vName=false;
        for(Vehicle car : myCar){
            if(!vName){
                System.out.println("\n您当前正在租用的"+car.getVehicleId()+"有: ");
                vName = true;
            }

            System.out.printf("%d %s %s %d辆\n", i, car.getVehicleId(), car.getBrand(), car.getUserRentNum());
            i++;
        }
    }

    @Override
    public void rentYourVehicle(){
        int carId;
        float moneyBeforePay = getMoney();
        float sumPay;
        int rentSum=0;
        while(true){
            System.out.print("请输入您想要租的车的编号, 或者输入-1，结束租车");
            carId = RentMgrSys.in.nextInt();
            if(carId == -1){
                sumPay = moneyBeforePay-getMoney();
                break;
            }else{
                rentSum += rentWhichCar(this, carId, rentSum);
            }
        }
        menuMsg.rentOver(sumPay, rentSum, getMoney());
    }

    @Override
    public void returnCar(String brand){
        try {
            RentMgrSys.rs = RentMgrSys.stmt.executeQuery("select * from userowncar where ownerId="+this.getUserId()+ " and brand='" +brand+ "'");
            String v="";
            if(RentMgrSys.rs.next()){
                v = RentMgrSys.rs.getString("vehicle");
            }

            RentMgrSys.rs.last();
            int row = RentMgrSys.rs.getRow();
            if(row == 0){
                return;
            }

            RentMgrSys.rs= RentMgrSys.stmt.executeQuery("select sum(nums) from userowncar where ownerId="+this.getUserId()+" and brand='"+brand+"'");
            RentMgrSys.rs.last();
            int returnNums = RentMgrSys.rs.getInt(1);

            RentMgrSys.rs = RentMgrSys.stmt.executeQuery("select nums from cardata where brand='"+brand+"'");
            int updateNums=0;

            if(RentMgrSys.rs.next()){
                updateNums = returnNums + RentMgrSys.rs.getInt("nums");
            }

            RentMgrSys.stmt.executeUpdate("update cardata set nums="+updateNums+" where brand='"+brand+"'");

            RentMgrSys.stmt.executeUpdate("delete from userowncar where ownerId="+this.getUserId()+" and brand='"+brand+"'");

            switch (v){
                case "轿车":
                    for(int i = 0; i < myCar.size(); i++){
                        if(myCar.get(i).getBrand().equals(brand)){
                            myCar.remove(i);
                        }
                    }
                    break;
                case "客车":
                    for(int i = 0; i < myBus.size(); i++){
                        if(myBus.get(i).getBrand().equals(brand)){
                            myBus.remove(i);
                        }
                    }
                    break;
                case "货车":
                    for(int i = 0; i < myTrunk.size(); i++){
                        if(myTrunk.get(i).getBrand().equals(brand)){
                            myTrunk.remove(i);
                        }
                    }
                    break;
                default:
                    break;
            }

            System.out.println("车辆归还成功, 能为您提供服务是我们的荣幸~~~~");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /*
    public void showMyCar(){
        System.out.println("轿车: ");
        for(int i=0; i < this.myCar.size(); i++){
            System.out.printf("%d:  %s %s %d辆\n", i, this.myCar.get(i).getBrand(), this.myCar.get(i).getType(), this.myCar.get(i).getUserRentNum());
        }

        System.out.println("\n客车: ");
        for(int i=0; i < this.myBus.size(); i++){
            System.out.printf("%d:  %s %s %d辆\n", i, this.myBus.get(i).getBrand(), this.myBus.get(i).getType(), this.myBus.get(i).getUserRentNum());
        }

        System.out.println("\n货车: ");
        for(int i=0; i < this.myTrunk.size(); i++){
            System.out.printf("%d:  %s %s %d辆\n", i, this.myTrunk.get(i).getBrand(), this.myTrunk.get(i).getType(), this.myTrunk.get(i).getUserRentNum());
        }
    }*/

    public void tobeVIP(){
        try {
            int count = RentMgrSys.stmt.executeUpdate(jdbcUtilsByAdmin.sqlChangeIdentify(this.getUserId()));
            RentMgrSys.rs = RentMgrSys.stmt.executeQuery("select money from usersinfo where id="+this.getUserId());
            if(RentMgrSys.rs.next()){
                float moneyBefore = RentMgrSys.rs.getFloat("money");
                RentMgrSys.stmt.executeUpdate("update usersinfo set money="+(moneyBefore-10000.0)+" where id="+getUserId());
            }

            if (count == 1){
                menuMsg.becomeVIP();
                this.setMoney(getMoney()-10000);
                this.setIdentity("VIP");
                RentMgrSys.logOut=true;
            }else{
                System.out.println("系统遇到错误，升级失败，请联系管理员处理");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void userMenu(){
        int choice;
        int num;
        while(true){
            menuMsg.printWelcome();
            num = getEachKindCarTotalNum(myBus)+getEachKindCarTotalNum(myCar)+getEachKindCarTotalNum(myTrunk);
            System.out.println("\n用户编号: "+getUserId()+"   用户身份: "+getIdentity()+"   账户余额: "+getMoney()+"   当前账户租车情况: "+num+"/"+this.rentCarLimitation);
            System.out.println("\n尊敬的顾客您好，欢迎来到鄙司租车系统，请问有什么能为您服务的吗？");
            System.out.print("0、还车\t1、租车\n2、查看账户下正在租用的车辆信息\t3、账户充值\n4、花费10000元升级为VIP, 享受更美观的界面、全场租车8折等服务\t5、退出系统\n6、退出登录");
            choice = RentMgrSys.in.nextInt();
            int i;
            switch (choice){
                case 0:
                    System.out.println("您好, 以下是您的账户中待归还车辆的信息:\n");
                    i = 1;
                    seeMyCarsDetails(myCar, i);
                    i += myCar.size();
                    seeMyCarsDetails(myBus, i);
                    i += myBus.size();
                    seeMyCarsDetails(myTrunk, i);
                    System.out.print("\n请输入您要归还的车辆的品牌, 一次只能归还一种: ");
                    String C = RentMgrSys.in.nextLine();
                    String brand = RentMgrSys.in.nextLine();
                    returnCar(brand);
                    break;
                case 1:
                    System.out.println("\n当前系统上架的车辆信息如下: ");
                    admin.showCarsInSystemNow();
                    rentYourVehicle();
                    break;
                case 2:
                    i = 1;
                    if(myCar!= null && myCar.size()!= 0){
                        seeMyCarsDetails(myCar, i);
                        i += myCar.size();
                    }
                    if(myBus!= null && myBus.size()!= 0){
                        seeMyCarsDetails(myBus, i);
                    }

                    if(myTrunk != null && myTrunk.size() != 0){
                        seeMyCarsDetails(myTrunk, i);
                    }
                    break;
                case 3:
                    menuMsg.addMoney();
                    float addMoney = RentMgrSys.in.nextFloat();
                    try {
                        int count = RentMgrSys.stmt.executeUpdate(jdbcUtilsByCostomer.sqlRecharge(this, this.getUserId(), addMoney));
                        if(count == 1){
                            setMoney(getMoney()+addMoney);
                            menuMsg.addMoneyOver(getMoney());
                        }else{
                            System.out.println("系统遇到了一些问题，充值失败，请联系管理员处理.");
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;
                case 4:
                    if (getMoney()>10000){
                        this.tobeVIP();
                    }else{
                        System.out.println("开通VIP服务需要支付10000元，您的账户余额不足！请充值后继续尝试。");
                    }
                    break;
                case 5:
                    thisLoginDataClear();
                    RentMgrSys.isOver=true;
                    break;
                case 6:
                    thisLoginDataClear();
                    RentMgrSys.logOut=true;
                    break;
            }
            System.out.println("\n");
            if(RentMgrSys.isOver || RentMgrSys.logOut){
                break;
            }
        }
    }
}