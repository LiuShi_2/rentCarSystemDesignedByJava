package CarRentalSystem.costomer;

import CarRentalSystem.CarInfo.Vehicle;

import java.util.ArrayList;

public interface aboutCostomerMenu {
    void userMenu();
    void seeMyCarsDetails(ArrayList<? extends Vehicle> vehicles, int i);
    void rentYourVehicle();
    void returnCar(String brand);
}
