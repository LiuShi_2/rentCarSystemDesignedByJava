package CarRentalSystem.costomer;
import CarRentalSystem.CarInfo.Vehicle;
import CarRentalSystem.JDBCUtils.*;
import CarRentalSystem.admin.admin;
import CarRentalSystem.RentMgrSys.RentMgrSys;
import CarRentalSystem.menuMsg.menuMsg;
import java.sql.SQLException;
import java.util.ArrayList;

public class VIPAboutCostomer extends User implements aboutCostomerMenu {
    public VIPAboutCostomer(int user, String identity, float money){ //创建普通用户账户
        super(user, identity, money);
    }


    @Override                                                     //查看用户当前车辆信息
    public void seeMyCarsDetails(ArrayList<? extends Vehicle> myCar, int i){

        boolean vName=false;
        for(Vehicle car : myCar){
            if(!vName){
                System.out.println("您当前正在租用的"+car.getVehicleId()+"有: ");
                vName = true;
            }

            System.out.println("编号: "+i+"\t\t车类: "+car.getVehicleId()+"\t\t品牌: "+car.getBrand()+"\t\t车牌号: "+car.getType()+"\t\t租用数量: "+car.getUserRentNum());
            i++;
        }
    }

    @Override
    public void rentYourVehicle(){
        int carId;
        float moneyBeforePay = getMoney();
        float sumPay;
        int rentSum=0;
        while(true){
            System.out.print("尊贵的VIP顾客您好，请输入您想要租的车的编号, 或者输入-1，结束租车");
            carId = RentMgrSys.in.nextInt();
            if(carId == -1){
                sumPay = moneyBeforePay-getMoney();
                break;
            }else{
                rentWhichCar(this, carId, rentSum);
            }
        }
        menuMsg.rentOver(sumPay, rentSum, getMoney());
    }

    @Override
    public void userMenu(){
        int choice;
        int num;
        while(true){
            menuMsg.printWelcome();
            num = getEachKindCarTotalNum(myBus)+getEachKindCarTotalNum(myCar)+getEachKindCarTotalNum(myTrunk);
            System.out.println("\n贵宾编号: "+this.getUserId()+"   贵宾身份: "+this.getIdentity()+"   贵宾账户余额: "+getMoney()+"   贵宾账户租车情况: "+num+"/"+this.rentCarLimitation);
            System.out.println("\n尊敬的VIP客户，您好，欢迎光临鄙司租车系统，请问有什么能为您服务的吗？");
            System.out.println("0、还车\n1、租车\n2、查看账户下正在租用的车辆信息\n3、账户充值\n4、查看公司仓库中各类车存量\n5、查看VIP权益\n6、退出登录\n7、关闭系统");
            choice = RentMgrSys.in.nextInt();
            int i;
            switch (choice){
                case 0:
                    System.out.println("尊贵的VIP客户, 您好, 以下是您的账户中待归还车辆的信息:\n");
                    i = 1;
                    seeMyCarsDetails(myCar, i);
                    i += myCar.size();
                    seeMyCarsDetails(myBus, i);
                    i += myBus.size();
                    seeMyCarsDetails(myTrunk, i);
                    System.out.print("\n请输入您要归还的车辆的品牌, 一次只能归还一种: ");
                    String c = RentMgrSys.in.nextLine();
                    String brand = RentMgrSys.in.nextLine();
                    returnCar(brand);
                    break;
                case 1:
                    menuMsg.ShowNowInfo();
                    admin.showCarsInSystemNow();
                    rentYourVehicle();
                    break;
                case 2:
                    System.out.println("尊贵的VIP客户, 您好, 这是鄙司正在为您服务的车辆的信息, 请您过目~~:");
                    i = 1;
                    seeMyCarsDetails(myCar, i);
                    i += myCar.size();
                    seeMyCarsDetails(myBus, i);
                    i += myBus.size();
                    seeMyCarsDetails(myTrunk, i);
                    break;
                case 3:
                    menuMsg.addMoney();
                    float addMoney = RentMgrSys.in.nextFloat();
                    try {
                        int count = RentMgrSys.stmt.executeUpdate(jdbcUtilsByCostomer.sqlRecharge(this, getUserId(), addMoney));
                        if(count == 1){
                            setMoney(getMoney()+addMoney);
                            menuMsg.addMoneyOver(getMoney());
                        }else{
                            System.out.println("尊贵的用户，系统遇到了一些问题，充值出现异常，请联系管理员处理.");
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;
                case 4:
                    admin.showCarsInSystemNow();
                    break;
                case 5:
                    System.out.println("尊贵的VIP客户，您当前享有的权益有: \n1、全场租车8折\t2、账户可同时租车上限提升至300\t3、VIP专属系统提示服务\t4、主页查看公司仓库中各类车存量");
                    break;
                case 6:
                    thisLoginDataClear();
                    RentMgrSys.logOut=true;
                    break;
                case 7:
                    thisLoginDataClear();
                    RentMgrSys.isOver=true;
                    break;
            }

            if(choice == 6 || choice == 7){
                System.out.println("尊贵的用户，为您服务是我们的荣幸，期待您再次光临~~~~~~\n");
                break;
            }
        }
    }


    @Override
    public void returnCar(String brand) {
        try {
            RentMgrSys.rs = RentMgrSys.stmt.executeQuery("select * from userowncar where ownerId=" + this.getUserId() + " and brand='" + brand+"'");
            String v="";
            if(RentMgrSys.rs.next()){
                v = RentMgrSys.rs.getString("vehicle");
            }
            RentMgrSys.rs.last();
            int row = RentMgrSys.rs.getRow();
            if (row == 0) {
                return;
            }

            RentMgrSys.rs = RentMgrSys.stmt.executeQuery("select sum(nums) from userowncar where ownerId=" + this.getUserId() + " and brand='" + brand+"'");
            RentMgrSys.rs.last();
            int returnNums = RentMgrSys.rs.getInt(1);

            RentMgrSys.rs = RentMgrSys.stmt.executeQuery("select nums from cardata where brand='" + brand+"'");
            int updateNums=0;
            if(RentMgrSys.rs.next()){
                updateNums = returnNums + RentMgrSys.rs.getInt("nums");
            }

            RentMgrSys.stmt.executeUpdate("update cardata set nums=" + updateNums + " where brand='" + brand+"'");
            RentMgrSys.stmt.executeUpdate("delete from userowncar where ownerId="+this.getUserId()+" and brand='"+brand+"'");

            switch (v) {
                case "轿车":
                    for (int i = 0; i < myCar.size(); i++) {
                        if (myCar.get(i).getBrand().equals(brand)) {
                            myCar.remove(i);
                        }
                    }
                    break;
                case "客车":
                    for (int i = 0; i < myBus.size(); i++) {
                        if (myBus.get(i).getBrand().equals(brand)) {
                            myBus.remove(i);
                        }
                    }
                    break;
                case "货车":
                    for (int i = 0; i < myTrunk.size(); i++) {
                        if (myTrunk.get(i).getBrand().equals(brand)) {
                            myTrunk.remove(i);
                        }
                    }
                    break;
                default:
                    break;
            }

            System.out.println("尊贵的VIP用户，车辆已经归还成功，鄙司能为您提供服务，是鄙司的荣幸，期待下次为您服务~~~~");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

