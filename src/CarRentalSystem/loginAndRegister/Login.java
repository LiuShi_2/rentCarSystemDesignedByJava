package CarRentalSystem.loginAndRegister;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Login {
    private int user = 0;
    private String passwords = "";


    public int getUser() {
        return user;
    }

    public String getPasswords() {
        return passwords;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public void setPasswords(String passwords) {
        this.passwords = passwords;
    }

    public Login(int user, String passwords, Statement stmt, ResultSet rs) {
        setUser(user);
        setPasswords(passwords);
    }


    public static class LoginJustifyResult{
        private int count;
        private int isTrue;
        private int userId;

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getIsTrue() {
            return isTrue;
        }

        public void setIsTrue(int isTrue) {
            this.isTrue = isTrue;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public LoginJustifyResult(){}
        public LoginJustifyResult(int count, int isTrue, int userId){
            setCount(count);
            setIsTrue(isTrue);
            setUserId(userId);
        }
    }

    /**
     * 判断用户输入的账号密码情况：正确、错误、不存在
     *
     * @return 一个 LoginJustifyResult 类， count用于判断输入的账号是否存在，isTrue判断输入的密码是否有误, userId是账号密码均正确时，才会传入的数据库中账号id，若不正常，则为-1
     * */
    public static LoginJustifyResult isLoginSuccess(int user, String passwords, Statement stmt, ResultSet rs) {

        //占位符，这里用使用数据库，判断输入的账号密码在数据库中是否有对应的，若没有，提示用户去注册
        //
        //public int judgeIdentity(User user){ }
        int count = 0, isTrue=0, userId = -1;
        String sqlSearch_User = "select * from usersinfo where account="+user;
        try {
            rs = stmt.executeQuery(sqlSearch_User);
        } catch (SQLException e) {
            e.printStackTrace();
        }


        try {
            if(rs.next()){
                count = rs.getRow();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        if(count == 1){
            String sqlIsTrue = "select * from usersinfo where account="+user;
            try {
                rs = stmt.executeQuery(sqlIsTrue);
            }catch (SQLException e){
                e.printStackTrace();
            }

            try {
                if(rs.next()){
                    if (rs.getString("passwords").equals(passwords)){
                        isTrue = 1;
                        userId = rs.getInt("id");
                        System.out.println("\n登陆成功");
                    }else{
                        System.out.println("密码错误，请检查后重新登录，若忘记密码，请联系管理员处理");
                        isTrue = 0;
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else if(count == 0){
            System.out.println("您输入的账号不存在，请检查您的账号是否输入有误，或进行注册");
        }

        return new LoginJustifyResult(count,isTrue,userId);
    }


}
