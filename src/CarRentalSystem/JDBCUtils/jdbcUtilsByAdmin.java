package CarRentalSystem.JDBCUtils;

import CarRentalSystem.RentMgrSys.RentMgrSys;
import java.sql.*;

/*
 * 对数据库操作方法的二次封装
 * 方便使用
 *
 * */
public class jdbcUtilsByAdmin{

    /**
     * 以下部分是管理员对数据库的CRUD操作
     * 因为考虑到不同环境下对获取到的数据的操作不同，所以这里只返回不同的sql语句，对返回数据的具体使用留在各个环境下各自实现
     * @return 除了查询用户信息, 其他返回为String, 对应的sql指令
     * */

    /**************************对用户的操作****************************************/
    //增

    public static String sqlSearchUserSpecifyInfo(String info, int id){
        return "select "+info+" from usersinfo where id="+id;
    }
    public static String sqlAddCostomer(int id, int account, String passwords, String identify, double money) {
        return "insert into usersinfo(id, account, passwords, identify, money) values("+id+", "+account+", "+"\""+passwords+"\", \""+identify+"\", "+money+")";
    }
    //将用户租用的车的信息存入数据库里用户仓库的表
    public static String sqlAddCarInfoIntoUserOwnCar(int ownerId, String vehicle, String brand, String type, int nums){
        return "insert into userOwnCar(ownerId, vehicle, brand, type, nums) values("+ownerId+", \'"+vehicle+"\', \'"+brand+"\', \'"+type+"\', "+nums+")";
    }


    //改(需要先查)
    public static String sqlChangeIdentify(int id){
        String sql="";
        try {
            RentMgrSys.rs = RentMgrSys.stmt.executeQuery(jdbcUtilsByAdmin.sqlSearchUserSpecifyInfo("identify", id));
            if(RentMgrSys.rs.next()){
                String identifyNow = RentMgrSys.rs.getString("identify");

                //用三元表达式修改用户身份标识
                identifyNow = identifyNow.equals("ordinary")?"VIP":"ordinary";
                sql = "update usersinfo set identify=\'"+identifyNow+"\' where id="+id;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return sql;
    }

    //查, 查到了直接打印出来
    //1、查询用户信息
    public static void showUsersAll(ResultSet rs){
        try {
            rs.last();
            int row = rs.getRow();
            if(row == 1){
                System.out.println("当前系统内没有客户");
                return;
            }

            rs.absolute(1);
            do{
                System.out.println("用户id: "+rs.getInt("id")+"      用户账号: "+rs.getInt("account")+"      用户密码: "+rs.getString("passwords")+"      用户身份: " +rs.getString("identify")+"      账上余额: "+rs.getFloat("money"));
            }while(rs.next());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //2、获取用户账号中正在租的车的信息
    public static String sqlReadSpecifyUserRentCarsInfo(String info,int id){
        return "select "+info+" from userOwnCar where ownerId="+id;
    }
    public static String sqlReadUserSpecificKindWhichCarInfo(String carKind, int id){
        return "select * from userOwnCar where ownerId="+id+" and vehicle=\'"+carKind+"\'";
    }
    public static String sqlReadSpecificKindCar(String carKind){
        return "select * from cardata where vehicle=\'"+carKind+"\'";
    }


    //读取当前收入
    public static String sqlReadMoneyNow(int id){
        return "select money from usersinfo where id="+id;
    }
    //更新营业额，需要先获取当前收入
    public static void UpdateMoney(float change, int id){
        try {
            RentMgrSys.rs = RentMgrSys.stmt.executeQuery(sqlReadMoneyNow(id));
            if(RentMgrSys.rs.next()){
                float money = RentMgrSys.rs.getFloat("money");

                money = id==0 ? money+change : money-change;

                int count = RentMgrSys.stmt.executeUpdate("update usersinfo set money="+money+" where id="+id);
                if (count==1 && id != 0){
                    System.out.println("付款成功！您所租的车辆即将开始为您服务~");
                }else if(count == 0 && id != 0){
                    System.out.println("遇到错误，付款失败，请重试");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    /************************对车辆的操作*************************/
    public static String sqlAddCar(int id, String vehicle, String brand, String type, int preRent, int nums, int features){
        return "insert into cardata(id, vehicle, brand, type, preRent, nums, feature) values('"+id+"', '"+vehicle+"', '"+brand+"', '"+type+"', "+preRent+", "+nums+", "+features+")";
    }

    public static String sqlDeleteCar(int id){
        return "delete from cardata where id="+id;
    }

    //修改车辆的数目
    public static String sqlChangeWhichCarNums(int id, int newNums){
        return "update cardata set nums="+newNums+" where id="+id;
    }

    //修改日租金
    public static String sqlChangeWhichCarPreRent(int id, int preRent){
        return "update cardata set preRent="+preRent+" where id="+id;
    }


    //查找车辆的一条指定信息
    public static String sqlSearchCarSpecifyInfo(String info, int id){
        return "select "+info+" from cardata where id="+id;
    }

    //修改车辆指定信息, 只支持修改余量或者日租金
    public static String sqlChangeCarSpecifyInfo(int id, String SInfo, int newValue){
        return "update cardata set "+SInfo+"="+newValue+" where id="+id;
    }

    /*
    public static void showWhichCar(ResultSet rs, int id){
        try {
            rs.absolute(id);
            String vehicle = rs.getString("vehicle");
            String brand = rs.getString("brand");
            String type = rs.getString("type");
            int preRent = rs.getInt("preRent");
            int nums = rs.getInt("nums");
            int feature = rs.getInt("feature");

            System.out.println("编号为"+id+"的车辆信息如下:\n车辆类别: "+vehicle+" 品牌: "+brand+" 车牌号："+type+" 仓库存量: "+nums);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }*/
}