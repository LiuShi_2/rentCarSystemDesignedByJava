package CarRentalSystem.JDBCUtils;

import CarRentalSystem.RentMgrSys.RentMgrSys;
import java.sql.SQLException;

public class DBLinkOver {
    public static void Over(){
        if (RentMgrSys.rs != null){
            try {
                RentMgrSys.rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(RentMgrSys.stmt != null){
            try {
                RentMgrSys.stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (RentMgrSys.conn != null){
            try {
                RentMgrSys.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
