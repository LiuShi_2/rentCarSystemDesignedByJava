package CarRentalSystem.JDBCUtils;
import CarRentalSystem.costomer.User;
import java.sql.ResultSet;
import java.sql.SQLException;

public class jdbcUtilsByCostomer{
    /***************************用户操作界面******************************/

    //用户租车
    public static String sqlUserRentCar(int carId, ResultSet rs, int nums){
        String res="";
        try {
            rs.absolute(carId+1);
            int numsNow = rs.getInt("nums");
            numsNow -= nums;
            res="update cardata set nums="+numsNow+" where id="+carId;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    //用户付款
    public static String sqlPayMoney(int userId, ResultSet rs, double rent){
        String res="";
        try {
            rs.absolute(userId+1);
            Double moneyNow=rs.getDouble("money");
            if(moneyNow < rent){
                System.out.println("余额不足！请充值后再继续租车!");
                res = "update usersinfo set money="+moneyNow+" where id="+userId;
            }else{
                moneyNow -= rent;
                res = "update userinfo set money="+moneyNow+" where id="+userId;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    //用户充值
    public static String sqlRecharge(User user , int userId, float addMoney){
        String res = "";
                float moneyNow = user.getMoney();
                moneyNow += addMoney;
                res = "update usersinfo set money="+moneyNow+" where id="+userId;
        return res;
    }

    //用户查看余额
    public static float SeeMoney(int userId, ResultSet rs){
        float moneyNow = (float) 0.0;

        try {
            rs.absolute(userId+1);
            moneyNow = rs.getFloat("money");
        }catch (SQLException e){
            e.printStackTrace();
        }

        return moneyNow;
    }
}
