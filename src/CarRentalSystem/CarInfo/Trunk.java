package CarRentalSystem.CarInfo;
import CarRentalSystem.RentMgrSys.RentMgrSys;
import CarRentalSystem.costomer.*;

public class Trunk extends Vehicle {
    private int load;//载重量，单位是 吨/t

    public Trunk(){}
    //设置Trunk的构造方法，用于接收菜单中选择的结果，new要的车
    public Trunk(String vehicleId, String brand, String type, int perRent, int nums, int load){
        //调用父类Vehicle的构造方法
        super(vehicleId, brand, type, perRent, nums, load);
    }

    //重写父类的付费方法
    @Override
    public float calRent(int numsAndDays, User user){
        System.out.print("请输入货车将要行驶的公里数: ");
        float rents = (float)0.0;
        int kilos = RentMgrSys.in.nextInt();

        //出于货车载重量的考虑，这里根据载重量的不同档位设置相应的收费标准
        if(getFeature() <= 10){
            rents = getFeature() * kilos * (float)0.08 * (float)user.getDiscount()*numsAndDays;
        }else if(getFeature() <= 40){
            rents = (10*(float)0.08 + (getFeature() - 10)*(float)0.12 * (float)user.getDiscount()*numsAndDays)*kilos;
        }else if(getFeature() > 40){
            rents = (10*(float)0.08 + 30*(float)0.12 + (getFeature() - 30)*(float)0.2 * (float)user.getDiscount()*numsAndDays)*kilos;
        }

        return rents;
    }
}
