package CarRentalSystem.CarInfo;

import CarRentalSystem.costomer.*;

public class Vehicle {
    private String vehicleId;
    private String brand;
    private int perRent;
    private String type;
    private int userRentNum=0;
    private int feature = 0;

    /* setter 和 getter */
    //车辆种类的,比如玛莎拉蒂、劳斯莱斯
    public String getVehicleId() {
        return vehicleId;
    }
    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }
    //车辆具体品牌的、比如幻影
    public String getBrand() {
        return brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    //车辆日租金
    public int getPerRent() {
        return perRent;
    }
    public void setPerRent(int perRent) {
        this.perRent = perRent;
    }
    //车牌号
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public int getFeature() {
        return feature;
    }

    public void setFeature(int feature) {
        this.feature = feature;
    }

    public Vehicle(){}

    public Vehicle(String vehicleId, String brand, String type, int perRent, int userRentNum, int feature){
        setVehicleId(vehicleId);
        setBrand(brand);
        setPerRent(perRent);
        setType(type);
        setUserRentNum(userRentNum);
        setFeature(feature);
    }

    public int getUserRentNum(){
        return userRentNum;
    }

    public void setUserRentNum(int userRentNum){
        this.userRentNum = userRentNum;
    }


    //应付租金的抽象方法
    public float calRent(int days, User user){float rent = 0; return rent;};
}
