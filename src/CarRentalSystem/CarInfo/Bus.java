package CarRentalSystem.CarInfo;

import CarRentalSystem.costomer.*;

public class Bus extends Vehicle {

    public Bus(){}
    //设置Bus的构造方法，用于接收菜单中选择的结果，new要的车
    public Bus(String vehicleId, String brand, String type, int perRent, int nums, int seats){
        //调用父类Vehicle的构造方法
        super(vehicleId, brand, type, perRent, nums, seats);
    }

    @Override
    public  float calRent(int days, User user){
        float rents;

        if(days > 150){
            rents = getPerRent() * days * (float)0.7 * (float)user.getDiscount();
        }else if(days > 30){
            rents = getPerRent() * days * (float)0.8 * (float)user.getDiscount();
        }else if(days > 7){
            rents = getPerRent() * days * (float)0.9 * (float)user.getDiscount();
        }else{
            rents = getPerRent() * days * (float)user.getDiscount();
        }

        return rents;
    }
}
