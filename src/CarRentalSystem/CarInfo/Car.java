package CarRentalSystem.CarInfo;

import CarRentalSystem.costomer.*;

public class Car extends Vehicle {

    public Car(){}
    //设置Car的构造方法，用于接收菜单中选择的结果，new要的车
    public Car(String vehicleId, String brand,String type, int perRent, int nums, int features){
        //调用父类Vehicle的构造方法
        super(vehicleId, brand, type, perRent, nums, features);

    }


    @Override
    public float calRent(int days, User user){
        float rents;

        if(days > 150){
            rents = getPerRent() * days * (float)0.7 * (float)user.getDiscount();
        }else if(days > 30){
            rents = getPerRent() * days * (float)0.8 * (float)user.getDiscount();
        }else if(days > 7){
            rents = getPerRent() * days * (float)0.9 * (float)user.getDiscount();
        }else{
            rents = getPerRent() * days * (float)user.getDiscount();
        }

        return rents;
    }
}
