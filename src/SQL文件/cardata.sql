/*
 Navicat Premium Data Transfer

 Source Server         : carrentsystem
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : rentcarsystem

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 13/11/2022 20:24:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cardata
-- ----------------------------
DROP TABLE IF EXISTS `cardata`;
CREATE TABLE `cardata`  (
  `id` int NULL DEFAULT NULL,
  `vehicle` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `brand` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `type` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `preRent` int NULL DEFAULT NULL,
  `nums` int NULL DEFAULT NULL,
  `feature` int NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cardata
-- ----------------------------
INSERT INTO `cardata` VALUES (1, '轿车', '宝马', '川B-0001', 150, 512, -1);
INSERT INTO `cardata` VALUES (2, '轿车', '保时捷', '川A-0002', 200, 282, -1);
INSERT INTO `cardata` VALUES (3, '轿车', '劳斯莱斯幻影', '川B-0003', 500, 10, -1);
INSERT INTO `cardata` VALUES (4, '轿车', '大众', '川A-0004', 100, 599, -1);
INSERT INTO `cardata` VALUES (5, '客车', '金杯-20座', '川B-0005', 80, 290, 20);
INSERT INTO `cardata` VALUES (6, '客车', '金杯-30座', '川B-0006', 100, 295, 30);
INSERT INTO `cardata` VALUES (7, '客车', '金龙-25座', '川A-0007', 100, 350, 25);
INSERT INTO `cardata` VALUES (8, '客车', '金龙-40座', '川B-0008', 120, 250, 40);
INSERT INTO `cardata` VALUES (9, '货车', '大卡one-限载60t', '川B-0009', 70, 300, 60);
INSERT INTO `cardata` VALUES (10, '货车', '大卡two-限载80t', '川B-0010', 100, 294, 80);
INSERT INTO `cardata` VALUES (12, '轿车', '迈巴赫', '川B-0012', 230, 60, -1);
INSERT INTO `cardata` VALUES (13, '客车', '金杯-35座', '川A-1020', 110, 150, 35);
INSERT INTO `cardata` VALUES (11, '轿车', '法拉利', '川B-1505', 240, 60, -1);

SET FOREIGN_KEY_CHECKS = 1;
